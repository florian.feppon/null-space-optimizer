Citation    
========

Please cite the following reference when using this package:

 
.. pull-quote:: 

    Feppon F., Allaire G. and Dapogny C. *Null space gradient flows for
    constrained optimization with applications to shape optimization.* 2020.
    ESAIM: COCV, 26 90
    `doi:10.1051/cocv/2020015 <https://doi.org/10.1051/cocv/2020015>`_
        
.. pull-quote::

   Feppon F. *Density based topology optimization with the Null Space Optimizer: a
   tutorial and a comparison* (2024).   
   `Structural and Multidisciplinary Optimization, 67(4), 1-34. <https://link.springer.com/article/10.1007/s00158-023-03710-w>`_. 


.. code-block:: bibtex

    @article{feppon2020optim,
       author = {{Feppon, F.} and {Allaire, G.} and {Dapogny, C.}},
       doi = {10.1051/cocv/2020015},
       journal = {ESAIM: COCV},
       pages = {90},
       title = {Null space gradient flows for constrained optimization with applications to shape optimization},
       url = {https://doi.org/10.1051/cocv/2020015},
       volume = 26,
       year = 2020
    }

        
    
        
.. code-block:: bibtex  
   
   @article{Feppon2024density,
      title     = "Density-based topology optimization with the Null Space Optimizer: a tutorial and a comparison",
      author    = "Feppon, Florian",
      journal   = "Structural and Multidisciplinary Optimization",
      publisher = "Springer",
      volume    =  67,
      number    =  1,
      pages     = "1--34",
      month     =  jan,
      year      =  2024
   }

   
    
